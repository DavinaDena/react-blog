# Project Blog 
#### Back and Front End Application
##### Project Budget but stepping it up a notch 


[Maquette and User Case](https://www.figma.com/file/iNat6IiFLD1FxJPccQPG0B/maquete-blog?node-id=0%3A1)

This project is quite similar to the previous one, the difference being that is a bit more complex due to the new concepts such like:

1. Back-end
    - Introduction of JWT (token)
    - Introduction of bcrypt
2. Front-end
    - Introduction of GlobalState
    - Recovery of Token
    - Slices, Thunks and Dispatches

### Back-End
For the back end, I worked with two tables on SQL. One being the **user** and the other being the **post**.

For the table **user** I did -> username, email, password, role and id.
For the table **post** I did -> title, text, date, postID, blogUser_id (foreign key) and blogUser.

So, a bit like the last project, this time:
1. Created a .env to connect to the database
2. On the file App.js, I created the const port and imported "dotenv-flow". I also generated a token, so I could manipulate some requests later.
3. Created the file Server.js and a const server that uses express. This time I also had to insert the function configurePassport and also tell the server to use 2 controllers.
4. Made 2 entities for user and post
5. Created the file Connection.js on the Repository and imported a Pool and also connected with my .env.
6. Did 2 repositories much like the last project but this time connecting the two tables with 'join'.
7. **Now** we also created a folder config where we inserted our public and private keys, so we can use them to make our token. 
8. Before starting the controller, I created a folder named utils and inserted two functions 
- One to generate the token and
- One to protect the routes.
``` JavaScript
export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: 60 * 60 });
    return token;
}
```
- Also did a function to convert the date (did this, so I can extract the date of the future posts without sql being too mad)
9. After this, I did the controller for both entities, but with major changes on the user controller. Since now we are working with registration and login, we need to make it more safe for the users. This is where the token come to use. Like this, we can stock some info locally (browser) that allows our user to not have to log in every time he changes page. Also is convenient for our security since we are not always making requests to our server, meaning we are not allowing vital info to pass around so often.
```JavaScript
if(samePassword){
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
```
After testing everything with thunderClient and checking that every request worked, I decided to pass to the front-end development.

### Front-End

Since we inserted new concepts for the back-end, we were obligated to also do it for the front. Not only because of the token but also, to facilitate our lives better, we learned about the **global state** of the app.

How to? Well...
1. Once again, starting with the .env.
2. **Created** the folder store where i did the auth(user)Slice, postSlice and store. The slices for the actions and, the store to gather the slices so we can dispatch.
3. On the index, we call our store to be used.
4. Created the pages needed for the development of the app
5. Created the necessary components to render on the pages.

Even tho I had a lot of difficulties during the project and, also acknowledge that in two months I would not have the capacity of replicating or doing all alone without notes, this has been a great project to have a general knowledge about security of the user and local/global states of an app and when to use them.

___
### Sneak Peek... token!
So we need to insert the keys on our folder config and then call them with variables like this:
```JavaScript
const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');
```

After having our keys, then we make a function that will generate a token based on the payload and also how long this token will last:
```JavaScript
export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: 60 * 60 });
    return token;}
```

Now that we have the token generated, we make a function so that we can get the token on the header and compare its info to the user (here we are comparing to the email of the user):
```JavaScript
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        try {
            const user = await UserRepository.findByEmail(payload.email);
            if (user) {
                return done(null, user);}
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);}}))}
```

Here we have the controller for login, and it's here where we are going to generate the token:
```JavaScript
userController.post('/login', async (req, res)=>{
    try {
        const user = await UserRepository.findByEmail(req.body.email);
        if(user){
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword){
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                        role: user.role})});
                return;}}
```
For the front, we need this function to put our token on the localStorage and where to get it (headers):
```JavaScript
axios.interceptors.request.use((config)=>{
    const token = localStorage.getItem('token');
    if(token){
        config.headers.authorization = 'bearer '+ token; }
    return config;})
```
And now we just need to also apply on our slices and thunks, here is the thunk to allow our user to login with the token: 
```JavaScript
export const loginWithToken = () => async (dispatch)=>{
    const token = localStorage.getItem('token')
    if(token){
        try {
            const user = await UserAuthService.fetchAccount()
            dispatch(login(user))
        } catch (error) {
            dispatch(logout())}
    }else{
        dispatch(logout())}}
```