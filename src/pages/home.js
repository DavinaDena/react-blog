import { Card } from "antd"
import { useSelector } from "react-redux"
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
//import { PostsByOneUser } from "../components/postsByOneUser"


export function Home() {
    const user = useSelector(state => state.auth.user)

    return (
        <Card>
            <h1>Welcome</h1>
            <Avatar size={64} icon={<UserOutlined />} />
            

            {user && <h3>{user.username}</h3>}



            {/* <PostsByOneUser/> */}
        </Card>
    )
}