import {  ListPosts } from "../components/listPosts";
import { SearchBar } from "../components/searchbar";


export function Posts(){


    return(
        <div>
            <SearchBar />
            <ListPosts/>
            
        </div>
    )
}