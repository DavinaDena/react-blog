import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchPostsByUser, userPost } from "../stores/post-slice"
import { deletePost } from '../stores/post-slice';

import { Row, Col } from 'antd';
import { Collapse } from 'antd';
import { Button } from 'antd';

const { Panel } = Collapse;



export function PostsByOneUser(){

    const dispatch= useDispatch()
    const user = useSelector(state => state.auth.user)
    const posts= useSelector(state=> state.post.userList)

    const onDelete = (id) => {
        dispatch(deletePost(id));
        dispatch(userPost(posts.filter(item => item.postID !== id)))
    }


    useEffect(()=>{
        dispatch(fetchPostsByUser())
    }, [dispatch])


    return(
        <>
        {user && posts.map(post => <Row key={posts.postID}>
            <br />
            <br />
            <br />
                <Col xs={{ span: 15, offset: 2 }} lg={{ span: 10, offset: 6 }}>
                    <Collapse accordion>
                        <Panel header={post.title} key={post.postID}>
                            <p>{post.text}</p>
                            <p>{new Intl.DateTimeFormat('en-GB',
                                {
                                    year: 'numeric',
                                    month: 'long',
                                    day: '2-digit'
                                }
                            ).format(new Date(post.date))}</p>

                            <br />
                            <Button type="primary" onClick={() => onDelete(post.postID)} danger>delete </Button>
                        </Panel>
                    </Collapse>
                </Col>

            </Row>)}
        </>
    )
}