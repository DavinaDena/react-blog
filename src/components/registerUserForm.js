import { Form, Input, Button } from 'antd';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { register } from '../stores/auth-slice';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons'

export default function RegisterUserForm() {

    const dispatch = useDispatch()

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        dispatch(register(values))
        setRedirect(true)
    }
    return (
        <div>
            <br />
            <br />
            <br />
            
            <Avatar size="large" icon={<UserOutlined />} />
                <h3>Register Here! :)</h3>
            {redirect ? (<Redirect pust to='/' />) : null}
            <Form
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 8 }}
                onFinish={onFinish}>
                
                {/* TO INSERT USERNAME */}
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}>
                    <Input />
                </Form.Item>

                {/* TO INSERT EMAIL */}
                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}>
                    <Input />
                </Form.Item>

                {/* TO INSERT PASSWORD */}
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}>
                    <Input.Password />
                </Form.Item>

                {/* BUTTON TO SUBMIT */}
                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}