
import { Row, Col } from 'antd';
import { Collapse } from 'antd';

const { Panel } = Collapse;


export function ShowPost({ post }) {
    


    return (
        <div>
            <Row>
                <Col xs={{ span: 15, offset: 2 }} lg={{ span: 10, offset: 6 }}>
                    <Collapse accordion>
                        <Panel header={post.title} key={post.postID}>
                            <p>{post.text}</p>
                            <p>{new Intl.DateTimeFormat('en-GB',
                                {
                                    year: 'numeric',
                                    month: 'long',
                                    day: '2-digit'
                                }
                            ).format(new Date(post.date))}</p>
                            <br />
                            <p>{post.blogUser.username}</p>
                        </Panel>
                    </Collapse>
                </Col>

            </Row>

        </div>
    )
}


