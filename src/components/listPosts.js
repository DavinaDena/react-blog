import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  fetchPosts } from "../stores/post-slice";
import { ShowPost } from "./showpost";




export function ListPosts() {
    const dispatch = useDispatch()
    const posts = useSelector(state => state.post.list)



    useEffect(() => {
        dispatch(fetchPosts())
    }, [dispatch])

    return (
        <div>
            {posts.map(post => <ShowPost key={post.postID} post={post} />)}

        </div>
    )
}