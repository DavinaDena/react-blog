import { Form, Input, Button } from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginWithCredentials } from '../stores/auth-slice';


export function LoginUserForm() {

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.login)

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        dispatch(loginWithCredentials(values))
        setRedirect(true)
    }

    return (
        <div>
            {redirect ? (<Redirect push to='/' />) : null}
            <br />
            <br />
            <br />

            <Form
                name="basic"
                labelCol={{ span: 5, offset: 3 }}
                wrapperCol={{ span: 8 }}
                onFinish={onFinish}

            >
                {feedback && <p>{feedback}</p>}
                
                {/* TO INPUT email */}
                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                {/* TO INPUT PASSWORD */}
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>


                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}