import { Form, Input, Button } from 'antd';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { addAPost } from '../stores/post-slice';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};

export function AddPost() {
    const dispatch = useDispatch()
    

    const [redirect, setRedict]= useState(false)

    const onFinish = (values) => {
        dispatch(addAPost(values))
        
        setRedict(true);
    };

    return (
        <div>
            {redirect? (<Redirect push to='/post'/>) : null}
            <br />
            <br />
            <Form {...layout} name="nest-messages" onFinish={onFinish} >
                <Form.Item
                    name="title"
                    label="title"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="text"
                    label="text">
                    <Input.TextArea />
                </Form.Item>
                <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

