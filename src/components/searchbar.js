import { Input, Space } from 'antd';
import { useDispatch } from 'react-redux';
import { fetchPosts } from '../stores/post-slice';

import { Row, Col } from 'antd';
// import { Collapse } from 'antd';

// const { Panel } = Collapse;

const { Search } = Input;


export function SearchBar() {

    const dispatch = useDispatch()

    const handleChange = (event) => {
        dispatch(fetchPosts(event.target.value));
    }


    return (
        <div>
            <br />
            <Row>
                <Col xs={{ span: 15, offset: 2 }} lg={{ span: 10, offset: 6 }}>
                    <Space direction="vertical">
                        <Search
                            placeholder="input search text"
                            allowClear
                            enterButton="Search"
                            size="large"
                            onChange={handleChange}
                        />
                    </Space>
                </Col>
            </Row >
            <br />
        </div>
    )
}