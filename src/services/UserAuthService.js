import axios from "axios";



export class UserAuthService {


    //to register
    static async register(user) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/user/register', user)
        return response.data
    }

    //to login
    static async login (credentials){
        const response= await axios.post(process.env.REACT_APP_SERVER_URL+'/api/user/login', credentials)
        localStorage.setItem('token', response.data.token)
        return response.data.user
    }

    static async fetchAccount(){
        //makes an error because its not him that passes the token
        // console.log(token);
        // const response = await axios.get('http://localhost:8000/api/user/account', {
        //     headers: {
        //         "Authorization" : `Bearer ${token}`
        //     }
        // })
        // return response.data

        let user = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/user/account');
        return user.data
    }

    static async getAllUsers(id){
        const response = await axios.get( process.env.REACT_APP_SERVER_URL+'/api/user', id)
        return response.data
    }
}