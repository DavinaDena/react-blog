import axios from "axios";



export class PostAuthService {

    static async getAllPost() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+ '/api/post/byUser')
        return response.data
    }

    static async searchPost(term){
        const response= await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post?search='+ term)
        return response.data
    }

    static async addPost(post) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/post/', post)
    }

    static async delPost(id){
        const del = await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/post/'+ id)
        return del;
    }

    static async getPostFromOneUser(){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/postByUser')
        return response.data
    }
}