
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AddPost } from './components/addPost';
import { Nav } from './components/nav';
import { Home } from './pages/home';
import Login from './pages/login';
import { Posts } from './pages/Posts';
import { PostsByUser } from './pages/PostsByUser';
import { Register } from './pages/register';
import { loginWithToken } from './stores/auth-slice';
import { fetchPosts } from './stores/post-slice';

function App() {

  const dispatch = useDispatch()

  useEffect(()=>{
    dispatch(loginWithToken())
    dispatch(fetchPosts())

  }, [dispatch])

  
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Nav/>
    
    <Switch>
      {/* POSTS */}
      <Route path="/post">
        <Posts/>
      </Route>
      {/* REGISTER */}
      <Route path="/register">
        <Register/>
      </Route>
      {/* LOGIN */}
      <Route path="/login">
        <Login/>
      </Route>
      {/* HOME */}
      <Route path="/" exact>
        <Home/>
      </Route>

      <Route path="/addPost" >
        <AddPost/>
      </Route>

      <Route path="/postsUser" >
        <PostsByUser/>
      </Route>
    </Switch>

    </BrowserRouter>
  );
}

export default App;
