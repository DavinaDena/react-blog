import { createSlice } from "@reduxjs/toolkit"
import { PostAuthService } from "../services/PostAuthService";



const initialState = {
    list: [], 
    userList: []
}

const postSlice = createSlice({
    name: 'post',
    initialState,
    reducers: {
        setList(state, { payload }) {
            state.list = payload;
        },
        userPost(state, {payload}){
            state.userList = payload
        }
    }
})

export const { setList, userPost } = postSlice.actions;

export default postSlice.reducer;

export const fetchPosts = (term) => async (dispatch) => {
    try {
        let post;
        //WATCHOUT!!!!!
        //the name you give on this variable is very important
        //for you to dispatch!!! 
        //because I fucking tried to dispatch but it didnt recognized
        //because I didnt had the same name!!!
        //because when you const posts = useSelector(state => state.post.list
        //the state.post.list represents the post down here....
        //it really drived me maaaaaad!
        if (term) {
            post = await PostAuthService.searchPost(term)
        } else {
            post = await PostAuthService.getAllPost()
        }

        dispatch(setList(post))
    } catch (error) {
        console.log(error);
    }
}

export const fetchPostsByUser =(id)=> async (dispatch)=>{
    try {
        let post = await PostAuthService.getPostFromOneUser(id)
        dispatch(userPost(post))
    } catch (error) {
        console.log(error);
    }
}

    export const addAPost = (values) => async (dispatch) => {
        try {
            await PostAuthService.addPost(values)
            dispatch(fetchPosts())
        } catch (error) {
            console.log(error)
        }
    }


    export const deletePost = (id) => async(dispatch)=>{
        try {
            await PostAuthService.delPost(id)
            
        } catch (error) {
            console.log(error)
        }
    }