import { createSlice } from "@reduxjs/toolkit";
import { UserAuthService } from "../services/UserAuthService";

//registration working!!!

const authSlice= createSlice({
    name:'auth',
    initialState:{
        user: false,
        registerFeedback:'',
        loginFeedback:''
    },
    //reducer to register
    reducers: {
        updateRegisterFeedback(state, {payload}){
            state.registerFeedback= payload
        },
        login(state, {payload}){
            state.user= payload;
        },
        updateLoginFeedback(state, {payload}){
            state.loginFeedback= payload
        },
        logout (state){
            state.user=null;
            localStorage.removeItem('token');
        }
    }
})


export const { updateRegisterFeedback, login, updateLoginFeedback, logout}= authSlice.actions;


export default authSlice.reducer;


//thunk to register
export const register = (body) => async (dispatch)=>{
    try {
        const { token}= await UserAuthService.register(body)
        localStorage.setItem('token', token);
        dispatch(updateRegisterFeedback('regis ok'))
        dispatch(loginWithCredentials(body))
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error))
    }
}

//thunk to login "manually"
export const loginWithCredentials= (credentials) => async (dispatch) =>{
    try {
        const user = await UserAuthService.login(credentials)
        dispatch(updateLoginFeedback('logi sycc'))
        dispatch(login(user))

    } catch (error) {
        dispatch(updateLoginFeedback('incorrect email or password'))
    }
}

// //login with token
// export const loginWithToken = () => async (dispatch)=>{
//     const token = localStorage.getItem('token')
//     if(token){
//         try {
//             const user = await 
//             dispatch(login(user))
//         } catch (error) {
//             console.log(error);
//         }
//     }else{
//         dispatch(logout())
//     }
// }

export const loginWithToken = () => async (dispatch) => {
    try {
        let token = localStorage.getItem('token')
        if (token) {
            let data = await UserAuthService.fetchAccount(token);
            if (data) {
                dispatch(login(data))
                
            }
        }
    } catch (error) {
        dispatch(logout())
    }
}
